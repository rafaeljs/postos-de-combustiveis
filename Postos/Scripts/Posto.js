﻿$(document).ready(function () {
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution:
            '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors' +
            ', Tiles courtesy of <a href="https://geo6.be/">GEO-6</a>',
        maxZoom: 18
    }).addTo(map);
    map.on('click', onMapClick);
    $(".combustiveis").inputmask('decimal', {
        'alias': 'numeric',
        'groupSeparator': '.',
        'autoGroup': true,
        'digits': 4,
        'radixPoint': ",",
        'digitsOptional': false,
        'allowMinus': false,
        'prefix': 'R$ ',
        'placeholder': ''
    });
});
var map = L.map('map').setView([-20.467524, -54.615538], 11);

var popup = L.popup();

function onMapClick(e) {
    popup
        .setLatLng(e.latlng)
        .setContent("Coordenadas " + e.latlng.toString())
        .openOn(map);
    $("#latlong").val(e.latlng.lat + "," + e.latlng.lng);
    $("#latlong").removeClass("is-invalid");
}

function setMarker() {
    var latLong = $("#latlong").val().split(',');
    if (!isNaN(latLong[0]) && !isNaN(latLong[1])) {
        popup
            .setLatLng({
                lat: parseFloat(latLong[0]),
                lng: parseFloat(latLong[1])
            })
            .setContent("Coordenadas LatLng(" + latLong[0] + "," + latLong[1] + ")")
            .openOn(map);
        $("#latlong").removeClass("is-invalid");
    }
    else {
        $("#latlong").addClass("is-invalid");
    }
}

function CadastrarPosto() {
    if (validarCampos()) {
        $('#modalCarregando').modal("show");
        var latLong = $("#latlong").val().split(',');
        $("#alertE").hide();
        $.ajax({
            type: "POST",
            url: "CadastrarPosto",
            data: {
                Nome: $("#nome").val(),
                gasComum: $("#gasComum").val().replace("R$",""),
                gasAdit: $("#gasAdit").val().replace("R$", ""),
                etanolComum: $("#etanolComum").val().replace("R$", ""),
                etanolAdit: $("#etanolAdit").val().replace("R$", ""),
                latitude: parseFloat(latLong[0]),
                longitude: parseFloat(latLong[1])
            },
            success: function (data) {
                $("#alertS").show();
                $('#modalCarregando').modal("hide");

            },
            error: function (data) {
                $("#alertE").show();
                $("#msgErro").text(data.Mensagem);
                $('#modalCarregando').modal("hide");
            }
        });
    }
    else {
        $("#alertE").show();
        $("#msgErro").text(" Um campo ou mais não esta preenchido corretamente!");
    }
}

function validarCampos() {
    var retorno = true;
    if ($("#nome").val() == "") {
        retorno = false;
        $("#nome").addClass("is-invalid");
    }
    else {
        $("#nome").removeClass("is-invalid");
    }

    if ($("#gasComum").val() == "" || $("#gasComum").val() == "R$ ,") {
        retorno = false;
        $("#gasComum").addClass("is-invalid");
    }
    else {
        $("#gasComum").removeClass("is-invalid");
    }

    if ($("#gasAdit").val() == "" || $("#gasAdit").val() == "R$ ,") {
        retorno = false;
        $("#gasAdit").addClass("is-invalid");
    }
    else {
        $("#gasAdit").removeClass("is-invalid");
    }

    if ($("#etanolComum").val() == "" || $("#etanolComum").val() == "R$ ,") {
        retorno = false;
        $("#etanolComum").addClass("is-invalid");
    }
    else {
        $("#etanolComum").removeClass("is-invalid");
    }

    if ($("#etanolAdit").val() == "" || $("#etanolAdit").val() == "R$ ,") {
        retorno = false;
        $("#etanolAdit").addClass("is-invalid");
    }
    else {
        $("#etanolAdit").removeClass("is-invalid");
    }
    if ($("#latlong").hasClass("is-invalid")) {
        retorno = false;
    }
    if ($("#latlong").val() == "") {
        retorno = false;
        $("#latlong").addClass("is-invalid");
    }
    return retorno;
}