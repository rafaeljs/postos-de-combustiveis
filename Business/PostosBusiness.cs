﻿using Contexto;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public class PostosBusiness
    {
        private readonly Context _contexto;
        public PostosBusiness ()
        {
            _contexto = new Context();
        }

        public List<PostoCombustivel> BuscarPostosMaisBaratos()
        {
            if (_contexto.PostoCombustivel.Any())
            {
                var gasComum = _contexto.PostoCombustivel.Where(x => x.CombustivelId == 1).OrderBy(x => x.Preco).FirstOrDefault();
                var gasAdit = _contexto.PostoCombustivel.Where(x => x.CombustivelId == 2).OrderBy(x => x.Preco).FirstOrDefault();
                var etanolComum = _contexto.PostoCombustivel.Where(x => x.CombustivelId == 3).OrderBy(x => x.Preco).FirstOrDefault();
                var etanolAdit = _contexto.PostoCombustivel.Where(x => x.CombustivelId == 4).OrderBy(x => x.Preco).FirstOrDefault();

                return new List<PostoCombustivel> {
                    gasComum,
                    gasAdit,
                    etanolComum,
                    etanolAdit
                };
            }
            return null;
        }
        public void CriarPosto(string Nome, decimal gasComum, decimal gasAdit, decimal etanolComum, decimal etanolAdit, decimal latitude, decimal longitude)
        {

            var novoPosto = new Posto
            {
                Nome = Nome,
                DataInclusao = DateTime.Now,
                Latitude = latitude,
                Longitude = longitude
            };

            var novoGasComum = new PostoCombustivel
            {
                Preco = gasComum,
                CombustivelId = 1
            };
            var novoGasAdit = new PostoCombustivel
            {
                Preco = gasAdit,
                CombustivelId = 2
            };
            var novoEtanolComum = new PostoCombustivel
            {
                Preco = etanolComum,
                CombustivelId = 3
            };
            var novoEtanolAdit = new PostoCombustivel
            {
                Preco = etanolAdit,
                CombustivelId = 4
            };
            novoPosto.PostoCombustivel.Add(novoGasComum);
            novoPosto.PostoCombustivel.Add(novoGasAdit);
            novoPosto.PostoCombustivel.Add(novoEtanolComum);
            novoPosto.PostoCombustivel.Add(novoEtanolAdit);
            _contexto.Posto.Add(novoPosto);
            _contexto.SaveChanges();
        }
    }
}
